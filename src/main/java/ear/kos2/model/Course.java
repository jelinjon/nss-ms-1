package ear.kos2.model;

import jakarta.persistence.*;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "course")
@Document(indexName = "course")
public class Course extends AbstractEntity {
    @Basic
    @Column(name = "name", nullable = false, length = 20)
    private String name;
    @Basic
    @Column(name = "credits", nullable = false)
    private Integer credits;
    @Basic
    @Column(name = "capacity", nullable = false)
    private Integer capacity;
    @ManyToMany
    @JoinTable(name = "teacher_course", joinColumns = @JoinColumn(name = "id_course"), inverseJoinColumns = @JoinColumn(name = "id_teacher"))
    private List<TeacherAccount> teachers;
    @ManyToMany
    @JoinTable(name = "student_course", joinColumns = @JoinColumn(name = "id_course"), inverseJoinColumns = @JoinColumn(name = "id_student"))
    private List<StudentAccount> students;

    @ElementCollection
    @Column(name = "class_ids", nullable = true)
    private List<Integer> classIds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public List<TeacherAccount> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<TeacherAccount> teachers) {
        this.teachers = teachers;
    }
    public List<StudentAccount> getStudents() {
        return students;
    }

    public void setStudents(List<StudentAccount> students) {
        this.students = students;
    }

    public List<Integer> getClasses() {
        return classIds;
    }

    public void setClasses(List<Integer> classes) {
        this.classIds = classes;
    }

    @Override
    public String toString() {
        return "Course{" +
                "name='" + name + '\'' +
                ", credits=" + credits +
                ", capacity=" + capacity +
                '}';
    }
}
