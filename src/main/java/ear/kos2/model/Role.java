package ear.kos2.model;

public enum Role {
    TEACHER("ROLE_TEACHER"), STUDENT("ROLE_STUDENT"), GUEST("ROLE_GUEST");

    private final String name;

    Role(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
