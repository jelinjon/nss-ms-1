package ear.kos2.service;

import ear.kos2.model.Role;
import ear.kos2.model.TeacherAccount;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

@Component
public class SystemInit {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInit.class);

    /**
     * Default admin username
     */
    private static final String ADMIN_USERNAME = "admin";
    private final TeacherAccountService teacherService;

    private final PlatformTransactionManager txManager;

    @Autowired
    public SystemInit(TeacherAccountService teacherService,
                             PlatformTransactionManager txManager) {
        this.txManager = txManager;
        this.teacherService = teacherService;
    }

    @PostConstruct
    private void initSystem() {
        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute((status) -> {
            generateTeacher();
            return null;
        });
    }

    /**
     * Generates an teacher account if it does not already exist.
     */
    private void generateTeacher() {
        if (teacherService.exists(ADMIN_USERNAME)) {
            LOG.info("admin admin account already exists");
            return;
        }
        final TeacherAccount teacher = new TeacherAccount();
        teacher.setUsername(ADMIN_USERNAME);
        teacher.setFirstName("Antonin");
        teacher.setLastName("Kmin");
        teacher.setPassword("admin");
        teacher.setRole(Role.TEACHER);

        System.out.println("Generated teacher user with credentials " + teacher.getUsername() + "/" + teacher.getPassword());
//        LOG.info("Generated teacher user with credentials " + teacher.getUsername() + "/" + teacher.getPassword());
        teacherService.persist(teacher);

    }
}

