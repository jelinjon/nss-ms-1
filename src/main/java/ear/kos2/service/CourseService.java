package ear.kos2.service;

import ear.kos2.dao.CourseDao;
import ear.kos2.model.Course;
import ear.kos2.model.StudentAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CourseService {

    private final CourseDao dao;

    @Autowired
    public CourseService(CourseDao dao) {
        this.dao = dao;
    }

    @Transactional(readOnly = true)
    public List<Course> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public List<Course> findAll(String name) {
        return dao.findAll(name);
    }

    @Transactional(readOnly = true)
    public List<Integer> getAllClasses(Course course) {
        return course.getClasses();
    }

    @Transactional(readOnly = true)
    public List<StudentAccount> getAllStudents(Course course) {
        return course.getStudents();
    }

    @Transactional(readOnly = true)
    public Course find(Integer id) {
        return dao.find(id);
    }
    @Transactional(readOnly = true)
    public List<Course> findByName(String name) {
        return dao.findAll(name);
    }

    @Transactional
    public void persist(Course course) {
        dao.persist(course);
    }

    @Transactional
    public void update(Course course) {
        dao.update(course);
    }

    @Transactional
    public void remove(Course course){
        dao.remove(course);
    }
}
