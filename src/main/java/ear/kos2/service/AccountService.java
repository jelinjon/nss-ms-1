package ear.kos2.service;

import ear.kos2.dao.AccountDao;
import ear.kos2.dao.CourseDao;
import ear.kos2.model.Account;
import ear.kos2.model.Course;

import ear.kos2.model.StudentAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class AccountService {

    private final Account account = new StudentAccount();

    private final AccountDao accountDao;
    final CourseDao courseDao;


    @Autowired
    public AccountService(AccountDao accountDao, CourseDao courseDao) {
        this.accountDao = accountDao;
        this.courseDao = courseDao;
    }

    @Transactional
    public List<Course> searchCourses(String name){
        List<Course> result = new ArrayList<>();

        for (Course c:courseDao.findAll()) {
            if(Objects.equals(c.getName(), name)){
                result.add(c);
            }
        }

        if (result.size() != 0) {
            return result;
        }
        else {
            throw new IllegalArgumentException("No courses exist with such name");
        }
    }

    //todo replace with call to schedule service with account id
//    @Transactional
//    public Schedule showMySchedule(){
//        return account.getSchedule();
//    }
    @Transactional
    public void login(String username, String password){

        if(accountDao.accountExists(username, password)){
            account.setOnline(true);
            accountDao.update(account);
        }
        else {
            throw new IllegalArgumentException("invalid login credentials");
        }
    }
    @Transactional
    public void logout(){
        account.setOnline(false);
    }

    @Transactional
    public List<Account> findAll(){
        return accountDao.findAll();
    }

    @Transactional
    public List<Object[]> findAllObjects() {
        return accountDao.findAllObjects();
    }

    @Transactional(readOnly = true)
    public boolean exists(String username) {
        return accountDao.findByUsername(username) != null;
    }
}
