package ear.kos2.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Service
public class TestService {
    private static final Logger LOG = LoggerFactory.getLogger(TestService.class);
    private final RestTemplate restTemplate;

    @Autowired
    public TestService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    public List contactService2(Integer id) {
        String url = "http://localhost:8082/ClassService/some-endpoint/" + id;
        try {
            return restTemplate.getForObject(url, List.class);
        } catch (RestClientException e) {
//            LOG.debug("Error contacting Service2 at " + url, e);
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
