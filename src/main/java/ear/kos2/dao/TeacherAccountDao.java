package ear.kos2.dao;

import ear.kos2.model.TeacherAccount;
import jakarta.persistence.PersistenceException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TeacherAccountDao extends BaseDao<TeacherAccount>{
    protected TeacherAccountDao() {
        super(TeacherAccount.class);
    }


    public List<Object[]> findAllObjects() {
        try {
            return em.createQuery("SELECT username, password, firstName, lastName FROM "
                    + type.getSimpleName() + " e", Object[].class).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }
}
