package ear.kos2.dao;

import ear.kos2.model.Account;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceException;
import jakarta.persistence.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountDao extends BaseDao<Account>{

    protected AccountDao() {
        super(Account.class);
    }
    
    public boolean accountExists(String username, String password){
        String select = "SELECT ua FROM Account ua WHERE ua.username=:username and ua.password=:password";

        Query query = em.createQuery(select);
        query.setParameter("username", username);
        query.setParameter("password", password);
        Account a;
        a = (Account) query.getSingleResult();

        return a != null;
    }

    public Account findByUsername(String username) {
        try {
            return em.createNamedQuery("Account.findByUsername", Account.class).setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Object[]> findAllObjects() {
        try {
            return em.createQuery("SELECT username, password FROM "
                    + type.getSimpleName() + " e", Object[].class).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }
}
