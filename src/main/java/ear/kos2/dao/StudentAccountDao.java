package ear.kos2.dao;

import ear.kos2.model.StudentAccount;
import jakarta.persistence.PersistenceException;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class StudentAccountDao extends BaseDao<StudentAccount> {
    protected StudentAccountDao() {
        super(StudentAccount.class);
    }

    public List<Object[]> findAllObjects() {
        try {
            return em.createQuery("SELECT username, password FROM "
                    + type.getSimpleName() + " e", Object[].class).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }

    public List<StudentAccount> getAllStudentAccounts() {
        try {
            return em.createQuery("SELECT e FROM " + type.getSimpleName() + " e", StudentAccount.class).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }
}
