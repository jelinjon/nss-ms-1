package ear.kos2.rest;

import ear.kos2.dto.StudentAccountDTO;
import ear.kos2.exception.NotFoundException;
import ear.kos2.model.Course;
import ear.kos2.model.StudentAccount;
import ear.kos2.rest.util.RestUtils;
import ear.kos2.service.CourseService;
import ear.kos2.service.StudentAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/student")
//@PreAuthorize("permitAll()")
public class StudentAccountController {
    private static final Logger LOG = LoggerFactory.getLogger(StudentAccountController.class);
    private final StudentAccountService studentAccountService;
    private final CourseService courseService;
@Autowired
public StudentAccountController(StudentAccountService studentAccountService,
                                CourseService courseService)
{
    this.studentAccountService = studentAccountService;
    this.courseService = courseService;
}
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StudentAccountDTO> getStudents() {
        return studentAccountService.findAllStudents().stream().map(this::convertStudentToDTO).collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public StudentAccountDTO getStudent(@PathVariable Integer id) {
        final StudentAccount p = studentAccountService.findStudent(id);
        if (p == null) {
            throw NotFoundException.create("Student", id);
        }
        return convertStudentToDTO(p);
    }
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, path = "/create")
    public ResponseEntity<Void> createStudent(@RequestBody StudentAccount student) {
        studentAccountService.persist(student);
        LOG.debug("Created student {}.", student);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", student.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}/schedule", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getScheduleByUserId(@PathVariable Integer id) {
        return studentAccountService.getScheduleById(id);
    }



    //@PreAuthorize("hasAnyRole('ROLE_TEACHER', 'ROLE_STUDENT')")
    @PostMapping(value = "/{id}/join/course/{courseId}")
    public ResponseEntity<?> joinCourse(@PathVariable Integer id, @PathVariable Integer courseId) {
        Course course = null;
        StudentAccount user = null;
        try {
            course = courseService.find(courseId);
            user = studentAccountService.findStudent(id);
        } catch (Exception e1) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Course or user not found.");
        }
        try{
            List<StudentAccount> tempStudents = course.getStudents();
            List<Course>tempCourses = user.getCourses();

            if (!(tempCourses.contains(course) && tempStudents.contains(user))) {
                //logic for joining course
                tempStudents.add(user);
                course.setStudents(tempStudents);

                //no need, cascades
//                tempCourses.add(course);
////                user.setCourses(tempCourses);

                studentAccountService.update(user);
                courseService.update(course);
            } else {
                //logic for leaving course
                tempStudents.remove(user);
                course.setStudents(tempStudents);

//                tempCourses.remove(course);
//                user.setCourses(tempCourses);

                studentAccountService.update(user);
                courseService.update(course);
            }

            return ResponseEntity.status(HttpStatus.ACCEPTED).body("Participants found, course joined or left.");
        } catch (Exception e2) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Unexpected error ocurred.");
        }
    }

//    @PathVariable Integer student, @PathVariable Integer sclass
    @PostMapping(value = "/{id}/joinclass/{class_id}")
    public ResponseEntity<?> joinClass(@PathVariable Integer id, @PathVariable Integer class_id) {
        try{
            return studentAccountService.addToSchedule();
        } catch (Exception e2) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Unexpected error during join process for student id " + id + ", and class id " + class_id);
        }
    }

    //@PreAuthorize("hasAnyRole('ROLE_TEACHER', 'ROLE_STUDENT')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/{id}/leave/class/{classId}")
    public ResponseEntity<Void> leaveClass(@PathVariable Integer id, @PathVariable Integer classId) {
//        SClass sclass = null;
        Integer sclassId = null;
        StudentAccount user = null;
        try{
            //fetch course if from class service
            Integer courseId = 0;
//            sclass = classService.find(classId);
            user = studentAccountService.findStudent(id);

            Course course = courseService.find(courseId);

            //check if user has joined the corresponding course
            if(!courseService.getAllStudents(course).contains(user)){
                LOG.debug("Cannot leave a class of a course user is not in.");
                final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{}", user.getId());
                return new ResponseEntity<>(headers, HttpStatus.FORBIDDEN);
            }

//            Schedule tempSchedule = user.getSchedule();
//            List<SClass> tempClasses = tempSchedule.getClasses();

            //case where the class doesn't exist in schedule
//            if(!tempClasses.contains(sclass)){
//                LOG.debug("User is not in the class.");
//                final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{}", user.getId());
//                return new ResponseEntity<>(headers, HttpStatus.FORBIDDEN);
//            }
            
//            tempClasses.remove(sclass);
//            tempSchedule.setClasses(tempClasses);

//            scheduleService.update(tempSchedule);
            studentAccountService.update(user);

            LOG.debug("User {} successfully joined class {} in.", user, sclassId);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{}", user.getId());
            return new ResponseEntity<>(headers, HttpStatus.ACCEPTED);
        } catch (Exception e2) {
            LOG.debug("Join attempt unsuccessful");
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/home");
            return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
        }
    }

    public StudentAccountDTO convertStudentToDTO(StudentAccount s) {
        System.out.println(s.getFirstName());
        return new StudentAccountDTO(s);
    }
}