package ear.kos2.rest;

import ear.kos2.exception.NotFoundException;
import ear.kos2.model.Course;
import ear.kos2.model.TeacherAccount;
import ear.kos2.rest.util.RestUtils;
import ear.kos2.service.TeacherAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/teacher")
// PreAuthorize on class applies to all endpoints it declares
//@PreAuthorize("permitAll()")
public class TeacherAccountController {
    private static final Logger LOG = LoggerFactory.getLogger(TeacherAccountController.class);
    private final TeacherAccountService teacherAccountService;

    @Autowired
    public TeacherAccountController(TeacherAccountService teacherAccountService) {
        this.teacherAccountService = teacherAccountService;
    }

    /**
     * Logs in a student
     *
     * @param user Student data
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> login(@RequestBody TeacherAccount user) {
        try {
            teacherAccountService.login(user.getUsername(), user.getPassword());
            LOG.debug("User {} successfully logged in.", user);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } catch (Exception e) {
            LOG.debug("Login unsuccessful");
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/home");
            return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/classes")
    public List<Course> getClasses() {
        return teacherAccountService.showMyCourses();
    }


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TeacherAccount getTeacher(@PathVariable Integer id) {
        final TeacherAccount p = teacherAccountService.find(id);
        if (p == null) {
            throw NotFoundException.create("Teacher", id);
        }
        return p;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Object[]> getTeachers() {
        return teacherAccountService.findAllObjects();
    }
}


