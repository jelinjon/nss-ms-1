package ear.kos2.rest;

import ear.kos2.exception.ValidationException;
import ear.kos2.model.Course;
import ear.kos2.model.StudentAccount;
import ear.kos2.repository.CourseRepository;
import ear.kos2.rest.util.RestUtils;
import ear.kos2.service.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/courses")
public class CourseController {
    private static final Logger LOG = LoggerFactory.getLogger(CourseController.class);

    private final CourseService courseService;
    private final CourseRepository courseRepository;

    @Autowired
    public CourseController(CourseService courseService, CourseRepository courseRepository) {
        this.courseService = courseService;
        this.courseRepository = courseRepository;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getCourses() {
        return courseService.findAll().stream().map(Course::toString).collect(Collectors.toList());
    }

    //@PreAuthorize("hasRole('ROLE_TEACHER')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createCourse(@RequestBody Course course) {
        courseService.persist(course); //normal DB persist
        courseRepository.save(course); //elasticsearch repository persist

        LOG.debug("Created course {}.", course);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", course.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCourse(@PathVariable Integer id) {
        // non elastic + elastic implementation
        try {
            final Optional<Course> p = courseRepository.findById(String.valueOf(id));
            if(p.isPresent()) {

                return ResponseEntity.status(HttpStatus.FOUND).body(p.toString());
            } else {

                final Course c = courseService.find(id);
                if (c == null) {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body("A course with that id was not found.");
                }
                return ResponseEntity.status(HttpStatus.FOUND).body(c.toString());
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Unexpected error during search ocurred.");
        }
    }

    // elastic search methods
    @GetMapping(value = "/search/name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Course> searchByName(@PathVariable String name) {
        return courseRepository.findByName(name);
    }
    @GetMapping(value = "/search/credits/{credits}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Course> searchByCredits(@PathVariable Integer credits) {
        return courseRepository.findByCredits(credits);
    }
    @GetMapping(value = "/search/capacity/{capacity}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Course> searchByCapacity(@PathVariable Integer capacity) {
        return courseRepository.findByCapacity(capacity);
    }
    @GetMapping("/search/teacher/{teacherId}")
    public List<Course> getByTeacherId(@PathVariable String teacherId) {
        return courseRepository.findByTeachers_Id(teacherId);
    }


    @PostMapping(value = "/create_test_course", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> insertCourseElastic() {
        try {
            Course temp = new Course();
            temp.setName("new elastic course");
            temp.setCredits(4);
            temp.setCapacity(40);
            courseService.persist(temp);
            courseRepository.save(temp);

            return ResponseEntity.status(HttpStatus.OK).body("Success");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Unexpected failure");
        }
    }

    // pre elastic name search
//    @GetMapping(value = "/search/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
//    public List<Course> getCourse(@PathVariable String name) {
//        final List<Course> p = courseService.findByName(name);
//        if (p == null) {
//            throw NotFoundException.create("Course", name);
//        }
//        return p;
//    }

    //@PreAuthorize("hasRole('ROLE_TEACHER')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCourse(@PathVariable Integer id, @RequestBody Course course) {
        final Course original = courseService.find(id);
        if (!original.getId().equals(course.getId())) {
            throw new ValidationException("Course identifier in the data does not match the one in the request URL.");
        }
        courseService.update(course);
        LOG.debug("Updated course {}.", course);
    }

    //@PreAuthorize("hasRole('ROLE_TEACHER')")
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeCourse(@PathVariable Integer id) {
        final Course toRemove = courseService.find(id);
        if (toRemove == null) {
            return;
        }
        courseService.remove(toRemove);
        LOG.debug("Removed course {}.", toRemove);
    }

    @GetMapping(value = "/{id}/classes", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Integer> getClassesByCourseId(@PathVariable Integer id) {
        final Course c = courseService.find(id);
        return courseService.getAllClasses(c);
    }

    //@PreAuthorize("hasRole('ROLE_TEACHER')")
    @GetMapping(value = "/{id}/students", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StudentAccount> getStudentsByCourseId(@PathVariable Integer id) {
        final Course c = courseService.find(id);
        return courseService.getAllStudents(c);
    }
}