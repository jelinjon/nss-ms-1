package ear.kos2.rest;

import ear.kos2.model.Account;
import ear.kos2.rest.util.RestUtils;
import ear.kos2.security.model.UserDetails;
import ear.kos2.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/account")
//@PreAuthorize("hasAnyRole('ROLE_TEACHER')")
//@PreAuthorize("permitAll()")
public class AccountController {
    private static final Logger LOG = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

//    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
//    public List<Account> getAccounts() {
//        return accountService.findAll();
//    }
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Object[]> getAccounts() {
    return accountService.findAllObjects();
}

    //@PreAuthorize("hasAnyRole('ROLE_TEACHER', 'ROLE_STUDENT')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/logout")
    public ResponseEntity<Void> logout() {
        try {
            //current.online = false

            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");
            return new ResponseEntity<>(headers, HttpStatus.I_AM_A_TEAPOT);
        } catch (Exception e) {
            LOG.debug("Login unsuccessful");
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/home");
            return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
        }
    }

    //@PreAuthorize("hasAnyRole('ROLE_TEACHER', 'ROLE_STUDENT', 'ROLE_GUEST')")
    @GetMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getCurrent(Authentication auth) {
        assert auth.getPrincipal() instanceof UserDetails;
        return ((UserDetails) auth.getPrincipal()).getUser().getFirstName() +
                " " + ((UserDetails) auth.getPrincipal()).getUser().getLastName() +
                " " + ((UserDetails) auth.getPrincipal()).getUser().getRole();
    }
}
