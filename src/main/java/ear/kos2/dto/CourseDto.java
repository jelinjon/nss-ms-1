package ear.kos2.dto;

import ear.kos2.model.AbstractEntity;
import ear.kos2.model.Course;
import ear.kos2.model.StudentAccount;
import ear.kos2.model.TeacherAccount;
import jakarta.persistence.*;

import java.util.List;
import java.util.stream.Collectors;

public class CourseDto {

    private String name;
    private Integer credits;
    private Integer capacity;
    private List<Integer> teachers;
    private List<Integer> students;
    private List<Integer> classIds;

    public CourseDto(Course c) {
        this.name = c.getName() ;
        this.credits = c.getCredits();
        this.capacity = c.getCapacity() ;
        this.teachers = c.getTeachers().stream().map(AbstractEntity::getId).collect(Collectors.toList());
        this.students = c.getStudents().stream().map(AbstractEntity::getId).collect(Collectors.toList());
        this.classIds = c.getClasses();
    }

    @Override
    public String toString() {
        return "CourseDto{" +
                "name='" + name + '\'' +
                ", credits=" + credits +
                ", capacity=" + capacity +
                ", teacher ids=" + teachers +
                ", student ids=" + students +
                ", classIds=" + classIds +
                '}';
    }
}
